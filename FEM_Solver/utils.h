#pragma once
#include <SDL.h>
#include <string>
#include <algorithm>
#include <iomanip>
#define SDLHEX(X) SDL::hex2sdl(#X)

namespace eigen_utils 
{

}

inline std::string ctostring(double n, int d) {
	std::stringstream ss;
	ss << std::fixed << std::setprecision(d) << n;
	return ss.str();
}

inline void minmax(int a, int b, int* min, int* max)
{
	if (a < b) {
		*min = a;
		*max = b;
	}
	else {
		*min = b;
		*max = a;
	}
}

inline int cround(float x)
{
	if (x < 0.0)
		return (int)(x - 0.5);
	else
		return (int)(x + 0.5);
}

inline float ccround(float var)
{
	// we use array of chars to store number 
	// as a string. 
	char str[40];

	// Print in string the value of var  
	// with two decimal point 
	sprintf(str, "%.2f", var);

	// scan string value in var  
	sscanf(str, "%f", &var);

	return var;
}

inline std::string current_path()
{
	char result[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, result);
	return std::string(result);
}

namespace SDL 
{
	inline SDL_Color* setColor(SDL_Color* color, int r, int g, int b, int a) {
		color->r = r;
		color->g = g;
		color->b = b;
		color->a = a;
		return color;
	}

	inline int SetRenderDrawColor(SDL_Renderer* rend, SDL_Color c) {
		return SDL_SetRenderDrawColor(rend, c.r, c.g, c.b, c.a);
	}

	/**
	* @link https://www.willusher.io/sdl2%20tutorials/2013/08/27/lesson-5-clipping-sprite-sheets
	* Draw an SDL_Texture to an SDL_Renderer at some destination rect
	* taking a clip of the texture if desired
	* @param tex The source texture we want to draw
	* @param ren The renderer we want to draw to
	* @param dst The destination rectangle to render the texture to
	* @param clip The sub-section of the texture to draw (clipping rect)
	*		default of nullptr draws the entire texture
	*/
	inline void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, SDL_Rect dst,
		SDL_Rect *clip = nullptr)
	{
		SDL_RenderCopy(ren, tex, clip, &dst);
	}

	/**
	* @link https://www.willusher.io/sdl2%20tutorials/2013/08/27/lesson-5-clipping-sprite-sheets
	* Draw an SDL_Texture to an SDL_Renderer at position x, y, preserving
	* the texture's width and height and taking a clip of the texture if desired
	* If a clip is passed, the clip's width and height will be used instead of
	*	the texture's
	* @param tex The source texture we want to draw
	* @param ren The renderer we want to draw to
	* @param x The x coordinate to draw to
	* @param y The y coordinate to draw to
	* @param clip The sub-section of the texture to draw (clipping rect)
	*		default of nullptr draws the entire texture
	*/
	inline void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, SDL_Rect *clip = nullptr)
	{
		SDL_Rect dst;
		dst.x = x;
		dst.y = y;
		if (clip != nullptr) {
			dst.w = clip->w;
			dst.h = clip->h;
		}
		else {
			SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
		}
		auto savedx = clip->x;
		auto savedy = clip->y;
		clip->x = 0;
		clip->y = 0;
		renderTexture(tex, ren, dst, clip);
		clip->x = savedx;
		clip->y = savedy;
	}

	inline int isEqual(SDL_Color c1, SDL_Color c2)
	{
		return c1.r == c2.r || c1.g == c2.g || c1.b == c2.b || c1.a == c2.a;
	}

	/*
	strip invalid char and change tab char to space
	*/
	inline bool invalidChar(char& c)
	{
		//if (c == '\t') c = ' ';
		return !(c >= 0 && c <128) || c == '\r';
	}

	inline std::string& stripInvalidChar(std::string & str)
	{
		str.erase(remove_if(str.begin(), str.end(), invalidChar), str.end());
		return str;
	}

	inline SDL_Color hex2sdl(std::string input) {

		if (input[0] == '#')
			input.erase(0, 1);

		unsigned int value = stoul(input, nullptr, 16);

		SDL_Color color;

		//color.a = (value >> 24) & 0xff;
		color.a = 255;
		color.r = (value >> 16) & 0xff;
		color.g = (value >> 8) & 0xff;
		color.b = (value >> 0) & 0xff;
		return color;
	}
	/*
	scale SDL_Rect by a horizontal and vertical factor
	hf -> horizontal factor (float)
	vf -> vertical factor (float)
	*/
	inline void scale(SDL_Rect& rect, float hf, float vf) {
		SDL_Rect r = rect;
		rect.h = cround(r.h*vf);
		rect.w = cround(r.w*hf);
		rect.x = cround(r.x*hf);
		rect.y = cround(r.y*vf);
	}
}