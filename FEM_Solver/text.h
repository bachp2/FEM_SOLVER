#pragma once
#include <SDL_ttf.h>
#include "TextBuffer.h"
#include <sstream>
#include "caret.h"
#include "utils.h"
#include "constants.h"
#include "font_glyphs.h"


#define FONT_CACHING
class Text {
public:
	void renderString(SDL_Renderer *rend, std::string str, int x, int y, SDL_Color color) {
		const int FONT_LINE_SKIP = TTF_FontLineSkip(font);
		int advance;
		TTF_GlyphMetrics(font, 'd', NULL, NULL, NULL, NULL, &advance);
		int i = 0;
		for (char c : str) {
			if (c == '\t') i += TABSIZE; 
			else {
				auto x = i*advance;
				font_glyphs.renderGlyph(rend, c, x, y, color);
				i++;
			}
		}
	}

	void free_texture(SDL_Texture *t)
	{
		if (t != nullptr) {
			SDL_DestroyTexture(t);
		}
	}

	void load_text_from_string_optimized(SDL_Renderer *rend, std::string input, const SDL_Color& colour, int linecount) {
		const int FONT_LINE_SKIP = TTF_FontLineSkip(font);
		int advance;
		TTF_GlyphMetrics(font, 'd', NULL, NULL, NULL, NULL, &advance);
		auto y = TOP_MARGIN + (linecount)*FONT_LINE_SKIP - focus_window.y;
		int i = 0;
		/*auto lnnum = std::to_string(linecount+1);
		for (char c : lnnum) {
			auto x = i*advance;
			font_glyphs.renderGlyph(rend, c, x, y);
			i++;
		}
		i = 0;*/
		for (char c : input) {
			if (c == '\t') i += TABSIZE;
			else {
				auto x = LEFT_MARGIN + i*advance;
				font_glyphs.renderGlyph(rend, c, x, y, colour);
				i++;
			}
		}
	}
#ifndef FONT_CACHING
	void load_text_from_string(SDL_Renderer *rend, std::string input, SDL_Color colour, int linelevel, int linecount)
	{
		//Get rid of preexisting texture
		free_texture(texture);
		//SDL_Surface* textSurface = TTF_RenderUTF8_Blended(font, input.c_str(), colour);
		//SDL_Surface* textSurface = TTF_RenderText_Blended_Wrapped(font, input.c_str(), colour, focus_window.w);
		SDL_Surface* textSurface = TTF_RenderText_Blended(font, input.c_str(), colour);
		if (textSurface != NULL)
		{
			//Create texture from surface pixels
			texture = SDL_CreateTextureFromSurface(rend, textSurface);
			if (texture == NULL)
			{
				printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
			}
			else
			{
				//Get image dimensions
				rect.x = LEFT_MARGIN;
				rect.y = TOP_MARGIN + linelevel*TTF_FontLineSkip(font);// TODO: create constriant for the scripting pane
				rect.w = textSurface->w;
				rect.h = textSurface->h;
			}

			//Get rid of old surface
			SDL_FreeSurface(textSurface);
		}
		else if (!input.empty())
		{
			printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
		}
	}
#endif // !FONT_CACHING
	template <class T>
	bool append_to_content(T in)
	{
		/*int ln, col;
		content.get_ln_col_at_point(ln, col);*/
		content.buff_insert(in);
		return true;
	}

	void scroll_down(mRect &scriptingPane, const int amount = 40) {
		focus_window.y += amount;
		if (focus_window.h + focus_window.y > scriptingPane.rect.h) {
			focus_window.y = scriptingPane.rect.h - focus_window.h;
		}
	}

	void scroll_up(mRect &scriptingPane, const int amount = 40) {
		focus_window.y -= amount;
		if (focus_window.y < scriptingPane.rect.y) {
			focus_window.y = 0;
		}
	}

	void draw_to_render_texture(SDL_Renderer *rend, mRect &scriptingPane, const bool& should_scroll_by_caret)
	{
		int i = 0;
		std::stringstream ss(content.str());
		std::string line;
		const int FONT_LINE_SKIP = TTF_FontLineSkip(font);
		int line_render_begin, line_render_end;
		line_render_begin = focus_window.y / FONT_LINE_SKIP;
		line_render_end = (focus_window.y + focus_window.h) / FONT_LINE_SKIP;
		while (std::getline(ss, line, NEWLINE)) {
			if (i >= line_render_begin && i <= line_render_end)
			{
#ifdef FONT_CACHING
				load_text_from_string_optimized(rend, line, default_color, i);
#else
				load_text_from_string(rend, line, default_color, i);
				SDL_RenderCopy(rend, texture, NULL, &(rect));
#endif // FONT_CACHING
			}
			i++;
		}
		auto entire_script_height = (i+1)*FONT_LINE_SKIP +TOP_MARGIN;
		if (entire_script_height > scriptingPane.rect.h) scriptingPane.rect.h = entire_script_height;

		while (i <= line_render_end) {
			font_glyphs.renderGlyph(rend, '~', LEFT_MARGIN, (i++)*FONT_LINE_SKIP - focus_window.y, SDLHEX(#CD4858));
		}

		if (should_scroll_by_caret)
		{
			auto scrolled = scroll_by_caret(scriptingPane);
			if (scrolled && entire_script_height < scriptingPane.rect.h) scriptingPane.rect.h -= FONT_LINE_SKIP;
			if (scriptingPane.rect.h < focus_window.h) scriptingPane.rect.h = focus_window.h;
		}
		//reset render target
		//SDL_SetRenderTarget(rend, NULL);
	}

	TextBuffer content;
	TTF_Font *font;
	SDL_Color default_color;
	SDL_Texture *render_texture;
	SDL_Rect focus_window;
	unsigned int size;
	Caret caret;
	FontGlyphs font_glyphs;
	 void destroy()
	{
		// Destroy textures;
#ifndef FONT_CACHING
		SDL_DestroyTexture(texture);
		texture = nullptr;
#endif // !FONT_CACHING
		SDL_DestroyTexture(render_texture);
		render_texture = nullptr;
		font_glyphs.~FontGlyphs();
		// Free global font
		TTF_CloseFont(font);
		font = nullptr;
	}
	 
private:
	int scroll_by_caret(mRect &scriptingPane) {
		if (caret.rect.y + caret.rect.h > focus_window.y + focus_window.h) {
			scroll_down(scriptingPane, caret.rect.y + caret.rect.h - focus_window.y - focus_window.h);
			return 1;
		}
		else if (caret.rect.y < focus_window.y) {
			scroll_up(scriptingPane, focus_window.y - caret.rect.y);
			return 1;
		}
		return 0;
	}
#ifndef FONT_CACHING
	SDL_Rect rect;
	SDL_Texture* texture;
#endif // !FONT_CACHING
};




