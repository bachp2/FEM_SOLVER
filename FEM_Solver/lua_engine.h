#pragma once
#include <lua.hpp>
#include <string>
#include <Windows.h>
#include "entities.h"

inline int PrintOut(lua_State *L, std::ostream& out);
inline int lua_Print(lua_State *L);

extern Truss truss;
class LuaEngine
{
public:
	void init();
	void run(std::string& script);
	void close();
	double get_global_double(const std::string& var);
private:
	lua_State* L;
	int AppendPath(const char* path)
	{
		lua_getglobal(L, "package");
		lua_getfield(L, -1, "path"); // get field "path" from table at top of stack (-1)
		std::string npath = path;
		npath.append(";");
		npath.append(lua_tostring(L, -1)); // grab path string from top of stack
		lua_pop(L, 1);
		lua_pushstring(L, npath.c_str());
		lua_setfield(L, -2, "path"); // set the field "path" in table at -2 with value at top of stack
		lua_pop(L, 1); // get rid of package table from top of stack
		return 0;
	}

	static void stackDump(lua_State *L) {
		int i;
		int top = lua_gettop(L);
		for (i = 1; i <= top; i++) {  /* repeat for each level */
			int t = lua_type(L, i);
			switch (t) {
			case LUA_TSTRING:  /* strings */
				printf("`%s'", lua_tostring(L, i));
				break;
			case LUA_TBOOLEAN:  /* booleans */
				printf(lua_toboolean(L, i) ? "true" : "false");
				break;
			case LUA_TNUMBER:  /* numbers */
				printf("%g", lua_tonumber(L, i));
				break;
			default:  /* other values */
				printf("%s", lua_typename(L, t));
				break;
			}
			printf("  ");  /* put a separator */
		}
		printf("\n");  /* end the listing */
	}
};
