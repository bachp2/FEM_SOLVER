#include "textbuffer.h"
#include <assert.h>
#include <string.h>
#include "constants.h"
TextBuffer::TextBuffer()
{
	size = 0;
	capacity = DEFAULT_BUFFER_SIZE;
	buffer = (char*) malloc((capacity+1)*sizeof(char)); //put aside a last slot for EOF
	point = &buffer[0];
	gapstrt = &buffer[0];
	gapend = &buffer[capacity-1];
	//buffer[capacity] = '\0';
	buffer[capacity] = '\n';
}

inline void minmax(int a, int b, int* min, int* max)
{
	if (a < b) {
		*min = a;
		*max = b;
	}
	else {
		*min = b;
		*max = a;
	}
}
void TextBuffer::delete_text(int pstart, int pend)
{
	int min, max;
	//assumption gap indexes are not on buffer pstart or pend
	minmax(pstart, pend, &min, &max);
	auto diff = max - min;
	if (&buffer[min] < gapstrt && &buffer[max] > gapend && size != capacity) diff -= get_gap_size();
	point = &buffer[max];
	focus_gap_on_point();
	for (int i = 0; i < diff; i++) {
		buff_throw();
	}
}

std::string TextBuffer::copy_text(int pstart, int pend) {
	int min, max;
	minmax(pstart, pend, &min, &max);
	if(&buffer[min] > gapend || &buffer[max] < gapstrt || size == capacity)
		return std::string(buffer+min, max - min);
	else {
		std::string sstr;
		for (int i = min; i < gapstrt - &buffer[0]; i++)
		{
			sstr.push_back(buffer[i]);
		}
		for (int i = gapend - &buffer[0]; i < max; i++)
		{
			sstr.push_back(buffer[i]);
		}
		return sstr;
	}
}

TextBuffer::~TextBuffer()
{
	free(buffer);
	buffer = NULL;
	point = NULL;
	gapstrt = NULL;
	gapend = NULL;
	size = 0;
	capacity = 0;
}

  void TextBuffer::move_point_left() {
	if (point - 1 == gapend && size != capacity) point -= get_gap_size() + 1;
	else if (point != begin()) point--;
	//if (*point == '\n') point--;
	//printf("gap start index %d, gap end %d, point %d\n", get_gap_start_index(), get_gap_end_index(), get_point_index());
	//printf("end: %d\n\n", end()-begin());
}

  void TextBuffer::move_point_right() {
	auto tmp = point;
	if ((point == gapstrt - 1 || point == gapstrt) && size != capacity) tmp += get_gap_size() + 1;
	else if (point != end()) tmp++;
	if (tmp <= end())
	{
		point = tmp;
	}
	//printf("gap start index %d, gap end %d, point %d\n", get_gap_start_index(), get_gap_end_index(), get_point_index());
	//printf("end: %d\n\n", end() - begin());
}

//NOTE: There is a bug affecting the movement to the fist line if the cursor's at the end of second line->FIXED

void TextBuffer::move_point_up() {
	auto tmp1 = point;
	if (seekc_backward('\n', tmp1))
	{
		auto col = point - tmp1 - 1;
		//account for gap
		if (point > gapend && tmp1 <= gapstrt && size != capacity) {
			col -= get_gap_size();
		}
		point = seekc_backward('\n', tmp1) ? tmp1 + 1 : begin();
		for (int i = 0; i < col; ++i) {
			if (*point == '\n') break;
			move_point_right();
		}
	}
}

void TextBuffer::move_point_down() {
	auto p = point;
	auto tmp1 = point;
	int col = 0;
	tmp1 = !seekc_backward('\n', p) ? begin() : p + 1;

	col = point - tmp1 + 1;
	//account for gap
	if (point > gapend && tmp1 <= gapstrt && size != capacity) {
		col -= get_gap_size();
	}
	if (*point != '\n') seekc_forward('\n', point);

	if (*point == '\n')
	{
		for (int i = 0; i < col; ++i) {
			move_point_right();
			if (*point == '\n') break;
		}
	}
}


/*
Since this routine is used in conjunction with move point. 
I'll make sure that the conditions are valid before using this method
TODO: implement positioning of gap at point when the point is moved by user input -> Testing phase
*/
void TextBuffer::focus_gap_on_point()
{
	if (point == gapstrt) return;
	if (size == capacity)
	{
		gapstrt = point;
		gapend = gapstrt - 1;
		return;
	}
	auto slot_moves = point - gapstrt;
	if (slot_moves > 0) {
		slot_moves -= get_gap_size();
		for (size_t i = 0; i < slot_moves; ++i)
		{
			*gapstrt = *(gapend + 1);
			gapstrt++;
			gapend++;
			clamp(gapstrt);
			clamp(gapend);
		}

		if (point = gapend+1) {
			point = gapstrt;
		}
	}
	else
	{
		slot_moves *= -1;
		for (size_t i = 0; i < slot_moves; ++i)
		{
			*gapend = *(gapstrt-1);
			gapstrt--;
			gapend--;
			clamp(gapstrt);
			clamp(gapend);
		}
	}
	assert(point == gapstrt);
	//assert(gapend == gapstrt + get_gap_size());
}

//NOTE: gap start will overstep gap end when the capacity is full
//I just don't feel the need to enforce gapstart <= gapend but it is a thing to keep in mind!
//On second thougth this issue need to be resolved since alot of the buffer logics depend on this invariant
//   s.Bach Phan 10:05PM 16-7-2018
int TextBuffer::buff_insert(char ch)
{
	if (point != gapstrt) focus_gap_on_point();
	if (size == capacity) {
		buff_expand(sizeof(int));
	}
	//insert char at point ptr
	*point = ch;
	//increase point position
	point++;
	//move gap start index
	gapstrt++;
	//may not be a potential solution!!!
	//if( gapstrt!=gapend ) gapstrt++;
	
	//increse size count
	size++;
	return 1;
}

int TextBuffer::buff_insert(std::string str)
{
	auto str_size = str.length();
	//For some unknown reason, the gap was not adjusting to the point after I moved it away by mouse cursor action 
	//it is especially noticeable when pasting text
	//I am too lazy to investigate further but this line do away with the bug
	if (gapstrt != point) focus_gap_on_point();
	//expand once
	if (str_size > get_gap_size())
		buff_expand(str_size);
	
	for (size_t i = 0; i < str_size; ++i) {
		buff_insert(str[i]);
	}
	return 1;
}

int TextBuffer::buff_throw()
{
	if (point != gapstrt) focus_gap_on_point();
	if (point == &buffer[0]) {
		return 0;
	}
	point--;
	gapstrt--;
	size--;
	return 1;
}

std::string TextBuffer::str()
{
	std::string str;
	auto iter = &buffer[0];
	while (iter != gapstrt) {
		str.push_back(*iter);
		iter++;
	}
	iter += get_gap_size();
	while (iter != &buffer[capacity]) {
		str.push_back(*iter);
		iter++;
	}
	//std string is not null terminated
	//str.push_back(*iter);
	return str;
}

std::string TextBuffer::str_with_gap()
{
	std::string str;
	auto iter = &buffer[0];
	while (iter != gapstrt) {
		str.push_back(*iter);
		iter++;
	}
	auto gap_size = get_gap_size();
	for (unsigned int i = 0; i < gap_size; ++i) {
		str.push_back('_');
	}
	iter += get_gap_size();
	while (iter != &buffer[capacity]) {
		str.push_back(*iter);
		iter++;
	}
	//std string is not null terminated
	//str.push_back(*iter);
	return str;
}


void TextBuffer::move_point(unsigned int nln, unsigned int ncol, unsigned int cln) {
	if (cln > nln)
	{
		while (cln > nln) {
			move_point_up();
			cln--;
		}
	}
	else if (cln < nln)
	{
		while (cln < nln) {
			move_point_down();
			cln++;
		}
	}

	move_point_to_begin_line();
	unsigned int c = 1;
	while (*point != '\n' && c < ncol) {
		if (*point == '\t')
			c += TABSIZE;
		else c++;
		move_point_right();
	}
}

void TextBuffer::move_point_to_begin_line() {
	if (seekc_backward('\n', point) == 0) point = begin();
	else point++;
}

void TextBuffer::move_point_to_end_line() {
	if (*point == '\n') return;
	if (seekc_forward('\n', point) == 0) {
		point = (end() - get_gap_size() == gapstrt) ? gapstrt : end();
	}
}

void TextBuffer::move_caret_to_point(unsigned int & cln, unsigned int & ccol) {
	auto ln = 1;
	auto p = point;
	while (seekc_backward('\n', p)) {
		ln++;
	}
	p = point;
	auto ref_pointer = seekc_backward('\n', p) ? p + 1 : &buffer[0];
	
	unsigned int col = 1;
	auto temp = ref_pointer;
	while (temp < point) {
		if (temp < gapstrt || temp > gapend || size == capacity) {
			if (*temp == '\t') col += TABSIZE;
			else col++;
		}
		temp++;
	}
	cln = ln;
	ccol = col;
}

int TextBuffer::seekc_backward(char ch, char*& cptr)
{
	auto old = cptr;
	while (cptr != &buffer[0]) {
		cptr--;
		if (cptr >= gapstrt && cptr <= gapend && size != capacity) continue;
		if (*cptr == ch) {
			return 1;
		}
	}
	cptr = old;
	return 0;
}

int TextBuffer::seekc_forward(char ch, char*& cptr)
{
	auto old = cptr;
	while (cptr != &buffer[capacity]) {
		cptr++;
		if (cptr >= gapstrt && cptr <= gapend && size != capacity) continue;
		if (*cptr == ch) {
			return 1;
		}
	}
	cptr = old;
	return 0;
}

void TextBuffer::buff_expand(int newslots)
{
	auto old_buff = buffer;
	auto newcap = capacity + newslots;
	buffer = (char*)malloc((newcap + 1)*sizeof(char));
	size_t c = 0;
	while (&old_buff[c] != gapstrt) {
		buffer[c] = old_buff[c];
		c++;
	}
	gapstrt = &buffer[c];
	point = gapstrt;

	c = 0;
	while (&old_buff[capacity - c] != gapend) {
		buffer[newcap - c] = old_buff[capacity - c];
		c++;
	}
	gapend = &buffer[newcap - c];
	auto gap_end_index = gapend - &buffer[0];
	capacity = newcap;
	free(old_buff);
}

