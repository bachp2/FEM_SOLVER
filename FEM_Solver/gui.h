#pragma once
#include <memory>
enum MouseBehavior { MOUSEFREE, ON_CLICKED, ON_DRAGGED, SCRNIN, SCRNOUT };
/*
@NOTE:
I am not satisfied with organizing the scrollbar as a component of Pane
sincethe scrollbar already form a relationship with Pane widget y having a pointer to its client widget
Having a container just to call its component seems needlessly complicated to me
*/
struct Pane : mRect {
	using mrectptr = std::unique_ptr<mRect>;
	bool render_flag = true;
	std::map<std::string, mrectptr> components;
	virtual void render(SDL_Renderer* rend) {
		if (render_flag) mRect::render(rend);
		for (auto &c : components) {
			c.second->render(rend);
		}
	}
};

struct HScrollBar : mRect {

};

struct VScrollBar : mRect {

	VScrollBar(Pane* pane, SDL_Rect* fw) {
		auto barwidth = SCROLLBAR_WIDTH;
		clientPane = pane;
		focus_window = fw;
		rect.x = pane->rect.x + pane->rect.w - barwidth;
		rect.y = 0;
		rect.w = barwidth;
		rect.h = pane->rect.h;;
		color = SDLHEX(#FFFFEA);
		thumb.rect.x = rect.x;
		thumb.rect.y = rect.y;
		thumb.rect.w = rect.w;
		thumb.rect.h = rect.h;
		thumb.color = SDLHEX(#5b5b4d);
	}

	~VScrollBar() {
		clientPane = nullptr;
	}

	void update() {
		float thumb_shaft_ratio = rect.h * 1.0f / clientPane->rect.h;
		thumb.rect.h = int(thumb_shaft_ratio * rect.h);
		thumb.rect.y = int(thumb_shaft_ratio * focus_window->y);
		if (thumb.rect.h < SMALLEST_THUMB_HEIGHT)
		{
			auto adjusty = cround(thumb.rect.y*(SMALLEST_THUMB_HEIGHT - thumb.rect.h)*1.0f / (rect.h - thumb.rect.h));
			thumb.rect.y -= adjusty;
			thumb.rect.h = SMALLEST_THUMB_HEIGHT;
		}
	}

	virtual void render(SDL_Renderer* rend) {
		mRect::render(rend);
		if (clientPane->rect.h > rect.h) {
			thumb.render(rend);
		}
		update();
	}
	mRect thumb;
private:
	Pane* clientPane;
	SDL_Rect* focus_window;
	const int SMALLEST_THUMB_HEIGHT = 10;
};



struct Mouse {
	int posX, posY;
	SDL_SystemCursor mid;
	SDL_Cursor* cursor;
	bool inWindow{ true };
	MouseBehavior state = MOUSEFREE;

	void free_cursor() {
		if (cursor != NULL) SDL_FreeCursor(cursor);
	}

	void setSystemCursor(SDL_SystemCursor id)
	{
		if (mid != id) {
			mid = id;
			free_cursor();
			cursor = SDL_CreateSystemCursor(id);
			SDL_SetCursor(cursor);
		}
	}

	void updateMouseState(HWND* winhandle) {
		assert(winhandle != NULL);
		POINT mpoint;
		GetCursorPos(&mpoint);
		ScreenToClient(*winhandle, &mpoint);
		posX = mpoint.x;
		posY = mpoint.y;
		//printf("windowheight:%d\n", wrect.bottom - wrect.top);
		if (posX < 0) posX = 0;
		if (posY < 0) posY = 0;
		//printf("mouse state :: x:%d , y:%d\n", posX, posY);
	}
};