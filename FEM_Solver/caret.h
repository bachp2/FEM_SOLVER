#pragma once
#include "mrect.h"
#include "constants.h"

//#include "text.h"
struct Caret : mRect 
{
public:
	bool should_scroll_by_caret = true;
	unsigned int current_line, current_column;
	bool mvisibility;
	unsigned int mblink_time;//in ms
	Caret() {
		baseticks = _getTicks();
		current_line = current_column = 1;
		mvisibility = true;
		mblink_time = (int) 500.f / DEFAULT_BLINKS_PER_SECOND;
	}

	Caret(TextBuffer *tb, bool visibility = true, int blinks_per_second = 2)
	{
		baseticks = _getTicks();
		current_line = current_column = 1;
		mvisibility = visibility;
		mblink_time = (int) 500.f / blinks_per_second;
		textBuffer = tb;
	}

	Caret(TextBuffer *tb, int blink_time, bool visibility = true)
	{
		baseticks = _getTicks();
		current_line = current_column = 1;
		mvisibility = visibility;
		mblink_time = blink_time;
		textBuffer = tb;
	}

	~Caret() { textBuffer = nullptr; }

	void move() {
		//move caret to a new position and update its tracking
		textBuffer->move_caret_to_point(current_line, current_column);
		baseticks = currticks;
		mvisibility = true;
		color.a = 255;
	}

	void move(int mX, int mY) {
		unsigned int mouseLn, mouseCol;
		getCaretPositionFromMouseCursor(mX, mY, mouseLn, mouseCol);
		textBuffer->move_point(mouseLn, mouseCol, current_line);
		textBuffer->move_caret_to_point(current_line, current_column);
	}

	virtual void render(SDL_Renderer *rend)
	{
		//update cursor

		//update position for non fixed width font
		//int iline, icol;
		//content.get_ln_col_at_point(iline, icol);
		//TTF_SizeText(font, content.get_current_line().c_str(), &cursor.rect.x, NULL);

		//update position for fixed width font
		int advance;
		TTF_GlyphMetrics(font, 'd', NULL, NULL, NULL, NULL, &advance);
		rect.x = LEFT_MARGIN + advance*(current_column - 1);
		//rect.y = topMargin + TTF_FontAscent(font) - rect.h;
		//adjusted to focus_window coordinate
		rect.y = TOP_MARGIN + (current_line - 1)*TTF_FontLineSkip(font) - focus_window->y;
		//do what its need to do
		blink();
		mRect::render(rend);
		//adjusted to real coordinate
		rect.y = TOP_MARGIN + (current_line - 1)*TTF_FontLineSkip(font);
	}

	void setTextBufferPointer(TextBuffer* tb) { textBuffer = tb; }
	void setFontPointer(TTF_Font* f) { font = f; }
	void setFocusWindowPointer(SDL_Rect* fw) { focus_window = fw; }
	void getCaretPositionFromMouseCursor(int mX, int mY, unsigned int& ln, unsigned int& col) {
		int advance;
		TTF_GlyphMetrics(font, 'd', NULL, NULL, NULL, NULL, &advance);
		ln = (focus_window->y + mY - TOP_MARGIN) / TTF_FontLineSkip(font) + 1;
		//col = cround((focus_window.x + mX - LEFT_MARGIN)*1.0f / advance) + 1;
		col = (focus_window->x + mX + advance/2 - LEFT_MARGIN) / advance + 1;
	}
private:
	static const int DEFAULT_BLINKS_PER_SECOND = 1;
	Uint32 baseticks = 0;
	Uint32 currticks = 0;
	TextBuffer *textBuffer;
	SDL_Rect* focus_window;
	TTF_Font* font;
	/*!
	\brief Internal wrapper to SDL_GetTicks that ensures a non-zero return value.

	\return The tick count.
	*/
	Uint32 _getTicks()
	{
		Uint32 ticks = SDL_GetTicks();

		/*
		* Since baseticks!=0 is used to track initialization
		* we need to ensure that the tick count is always >0
		* since SDL_GetTicks may not have incremented yet and
		* return 0 depending on the timing of the calls.
		*/
		if (ticks == 0) {
			return 1;
		}
		else {
			return ticks;
		}
	}
	
	void _switch(bool& in) { in = !in; }

	void _clamp(size_t& in, size_t min, size_t max) {
		assert(max >= min);
		if (in < min) in = 1;
		else if (in > max) in = max;
	}

	void blink() {
		currticks = _getTicks();
		auto diff = currticks - baseticks;
		if (diff >= mblink_time)
		{
			baseticks = currticks;
			_switch(mvisibility);
			color.a = mvisibility ? 255 : 0;
		}
	}
};