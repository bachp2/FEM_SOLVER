#pragma once
#include <cairo.h>
#include "entities.h"

class Canvas {

private:
	SDL_Texture* texture;
public:
	Truss* truss;
	void free_texture()
	{
		if (texture != nullptr) {
			SDL_DestroyTexture(texture);
		}
	}

	virtual void create_texture(SDL_Renderer* rend, SDL_Rect* target) {
		texture = SDL_CreateTexture(rend,
			SDL_PIXELFORMAT_ARGB8888,
			SDL_TEXTUREACCESS_STREAMING,
			target->w, target->h
		);
	}

	virtual void render(SDL_Renderer* rend, SDL_Rect* target) {
		//assert(texture != NULL);
		/*float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		printf("<%.2f,%.2f,%.2f>\n", r, g, b);*/
		void* pixels;
		int pitch;

		SDL_LockTexture(texture, NULL, &pixels, &pitch);
		auto cairo_surface = cairo_image_surface_create_for_data(
			(unsigned char*)pixels,
			CAIRO_FORMAT_ARGB32,
			target->w, target->h, pitch
		);

		auto cr = cairo_create(cairo_surface);

		cairo_select_font_face(cr, "monospace",
			CAIRO_FONT_SLANT_NORMAL,
			CAIRO_FONT_WEIGHT_BOLD);

		cairo_set_font_size(cr, 13);
		//Draw stuff
		
		cairo_set_source_rgb(cr, 1, 1, 1);
		cairo_paint(cr);
		cairo_set_line_width(cr, 1);
		cairo_set_source_rgb(cr, 0.69, 0.19, 0);

		//cairo_move_to(cr, truss->offsetX, truss->offsetY);
		//cairo_show_text(cr, "Hello");
		for (int i = 0; i < truss->elements.size(); i += 2) {
			auto ii = truss->elements[i];
			auto jj = truss->elements[i + 1];

			auto sx = truss->offsetX + truss->nodes_xy_position[ii*2] * truss->unit;
			auto sy = truss->offsetY - truss->nodes_xy_position[ii * 2 + 1] * truss->unit;
			
			auto tx = truss->offsetX + truss->nodes_xy_position[jj*2] * truss->unit;
			auto ty = truss->offsetY - truss->nodes_xy_position[jj*2+1] * truss->unit;
			cairo_move_to(cr, sx, sy);
			cairo_line_to(cr, tx, ty);
			cairo_stroke(cr);
		}

		cairo_set_source_rgb(cr, 0, 0, 0);
		for (int i = 0; i < truss->nodes_xy_position.size(); i += 2) {
			auto sx = truss->offsetX + truss->nodes_xy_position[i]*truss->unit;
			auto sy = truss->offsetY - truss->nodes_xy_position[i + 1] * truss->unit;
			cairo_arc(cr, sx, sy, 3, 0, 2 * M_PI);
			cairo_fill(cr);
			cairo_move_to(cr, sx+3, sy+3);
			cairo_show_text(cr, std::to_string(i/2+1).c_str());
		}


		for (auto& e : truss->forces) {
			auto i = e.first;
			auto f = e.second;
			//printf("i:%d, <%.2f,%.2f>\n", i, f[0], f[1]);
			auto sx = truss->offsetX + truss->nodes_xy_position[i*2] * truss->unit;
			auto sy = truss->offsetY - truss->nodes_xy_position[i*2 + 1]*truss->unit;
			cairo_move_to(cr, sx + 10, sy + 10);
			std::string str = ctostring(sqrt(f[0] * f[0] + f[1] * f[1]), 0) + " " + truss->force_unit;
			cairo_show_text(cr, str.c_str());
			cairo_move_to(cr, sx, sy);
			auto funit = truss->unit /100;
			cairo_rel_line_to(cr, f[0]* funit, -f[1]* funit);
			cairo_stroke(cr);
			double angle = atan2(f[1], f[0]) + M_PI;
			double x1, x2, y1, y2;
			auto end_x = sx + f[0] * funit;
			auto end_y = sy - f[1] * funit;

			x1 = truss->arrow_length * cos(angle - truss->arrow_angle);
			y1 = -truss->arrow_length * sin(angle - truss->arrow_angle);
			x2 = truss->arrow_length * cos(angle + truss->arrow_angle);
			y2 = -truss->arrow_length * sin(angle + truss->arrow_angle);
			cairo_move_to(cr, end_x, end_y);
			cairo_rel_line_to(cr, x1, y1);
			cairo_stroke(cr);
			cairo_move_to(cr, end_x, end_y);
			cairo_rel_line_to(cr, x2, y2);
			cairo_stroke(cr);
		}

		//Render
		SDL_UnlockTexture(texture);
		SDL::renderTexture(
			texture, rend, 
			target->x, target->y,
			target
		);
		cairo_destroy(cr);
		cairo_surface_destroy(cairo_surface);
	};
};
