#pragma once
#include <unordered_map>
#include <functional>
#define KEY_NAME(X) SDL_GetKeyName(X)
#define MAP_KEY(X) commands_map[SDL_GetKeyName(X)]
#define MAP_COMBO(X) commands_map[X]

class KeysHandler {
	//TODO: Convert this class into singleton structure
	typedef void(*ScriptFunction)(Text*, TextSelection*);
	typedef std::unordered_map<std::string, ScriptFunction> ComMap;
	ComMap commands_map;
	
	void add_commands() {
		MAP_KEY(SDLK_BACKSPACE) = &ev_backspace;
		MAP_KEY(SDLK_RETURN) = &ev_return;
		MAP_KEY(SDLK_LEFT) = &ev_mvl;
		MAP_KEY(SDLK_RIGHT) = &ev_mvr;
		MAP_KEY(SDLK_UP) = &ev_mvu;
		MAP_KEY(SDLK_DOWN) = &ev_mvd;
		MAP_KEY(SDLK_TAB) = &ev_tab;
		MAP_KEY(SDLK_HOME) = &ev_home;
		MAP_KEY(SDLK_END) = &ev_end;
		MAP_KEY(SDLK_KP_ENTER) = &ev_return;
		MAP_COMBO("Ctrl-C") = &ev_copy;
		MAP_COMBO("Ctrl-V") = &ev_paste;
		MAP_COMBO("Ctrl-S") = &ev_save;

		/*commands_map["Ctrl-V"] = &ev_end;
		commands_map["Ctrl-X"] = &ev_end;
		commands_map["Ctrl-L"] = &ev_end;
		commands_map["Ctrl-K"] = &ev_end;
		commands_map["Ctrl-A"] = &ev_end;*/
	}
public:
	KeysHandler() {
		add_commands();
	}

	void do_command(std::string com, Text* text, TextSelection* textSelection) {
		auto iter = commands_map.find(com);
		if (iter != commands_map.end()) {
			(*iter->second)(text, textSelection);
			//printf("\n%s", iter->first.c_str());
		}
	}

private:
	static void ev_backspace(Text* text, TextSelection* textSelection) {
		if (!text->content.empty()) {
			if (textSelection->selecting) {
				text->content.delete_text(textSelection->begin_point, textSelection->end_point);
				textSelection->selecting = false;
			}
			else {
				//lop off character
				text->content.buff_throw();
			}
			text->caret.move();
			text->caret.should_scroll_by_caret = true;
		}
	}

	static void ev_return(Text* text, TextSelection* textSelection) {
		if (textSelection->selecting) {
			text->content.delete_text(textSelection->begin_point, textSelection->end_point);
			textSelection->selecting = false;
		}
		text->append_to_content(NEWLINE);
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvr(Text* text, TextSelection* textSelection) {
		text->content.move_point_right();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvl(Text* text, TextSelection* textSelection) {
		text->content.move_point_left();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvu(Text* text, TextSelection* textSelection) {
		text->content.move_point_up();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvd(Text* text, TextSelection* textSelection) {
		text->content.move_point_down();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_tab(Text* text, TextSelection* textSelection) {
		//text->content.buff_insert(TAB_INDENT);
		if (textSelection->selecting) {
			text->content.delete_text(textSelection->begin_point, textSelection->end_point);
			textSelection->selecting = false;
		}
		text->content.buff_insert(TAB_INDENT);
		//text->content.buff_insert(SPACES_INDENT);
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_end(Text* text, TextSelection* textSelection) {
		text->content.move_point_to_end_line();
		text->caret.move();
		//text->caret.should_scroll_by_caret = true;
	}

	static void ev_home(Text* text, TextSelection* textSelection) {
		text->content.move_point_to_begin_line();
		text->caret.move();
		//text->caret.should_scroll_by_caret = true;
	}

	static void ev_paste(Text* text, TextSelection* textSelection) {
		if (textSelection->selecting) {
			text->content.delete_text(textSelection->begin_point, textSelection->end_point);
			textSelection->selecting = false;
		}
		if (SDL_HasClipboardText()) {
			auto cliptext = SDL_GetClipboardText();
			auto validclip = SDL::stripInvalidChar(std::string(cliptext));
			text->append_to_content(validclip);
			text->caret.move();
			SDL_free(cliptext);
		}
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_copy(Text* text, TextSelection* textSelection) {
		if (textSelection->selecting) {
			auto sstr = text->content.copy_text(textSelection->begin_point, textSelection->end_point);
			SDL_SetClipboardText(sstr.c_str());
		}
	}
	static void ev_save(Text* text, TextSelection* textSelection) {
		int msgboxID =  MessageBox(
			NULL, 
			"Do you want to save change to untitled?", 
			"FEM SOLVER", MB_YESNOCANCEL
		);

		switch (msgboxID)
		{
		case IDCANCEL:
			// do nothing
			break;
		case IDNO:
			// TODO: add code
			break;
		case IDYES:
			// TODO: add code
			////Save Dialog
			//OPENFILENAME ofn;
			//char szFileName[MAX_PATH] = "";

			//ZeroMemory(&ofn, sizeof(ofn));

			//ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
			//ofn.hwndOwner = mHandle;
			//ofn.lpstrFilter = "Lua Scripts (*.lua)\0*.lua\0Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
			//ofn.lpstrFile = szFileName;
			//ofn.nMaxFile = MAX_PATH;
			//ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			//ofn.lpstrDefExt = "lua";
			//if (GetSaveFileName(&ofn)) {

			//}#pragma once
#include <unordered_map>
#include <functional>
#define KEY_NAME(X) SDL_GetKeyName(X)
#define MAP_KEY(X) commands_map[SDL_GetKeyName(X)]
#define MAP_COMBO(X) commands_map[X]

class KeysHandler {
	//TODO: Convert this into a singleton structure
	typedef void(*ScriptFunction)(Text*, TextSelection*);
	typedef std::unordered_map<std::string, ScriptFunction> ComMap;
	ComMap commands_map;
	
	void add_commands() {
		MAP_KEY(SDLK_BACKSPACE) = &ev_backspace;
		MAP_KEY(SDLK_RETURN) = &ev_return;
		MAP_KEY(SDLK_LEFT) = &ev_mvl;
		MAP_KEY(SDLK_RIGHT) = &ev_mvr;
		MAP_KEY(SDLK_UP) = &ev_mvu;
		MAP_KEY(SDLK_DOWN) = &ev_mvd;
		MAP_KEY(SDLK_TAB) = &ev_tab;
		MAP_KEY(SDLK_HOME) = &ev_home;
		MAP_KEY(SDLK_END) = &ev_end;
		MAP_KEY(SDLK_KP_ENTER) = &ev_return;
		MAP_COMBO("Ctrl-C") = &ev_copy;
		MAP_COMBO("Ctrl-V") = &ev_paste;
		MAP_COMBO("Ctrl-S") = &ev_save;

		/*commands_map["Ctrl-V"] = &ev_end;
		commands_map["Ctrl-X"] = &ev_end;
		commands_map["Ctrl-L"] = &ev_end;
		commands_map["Ctrl-K"] = &ev_end;
		commands_map["Ctrl-A"] = &ev_end;*/
	}
public:
	KeysHandler() {
		add_commands();
	}

	void do_command(std::string com, Text* text, TextSelection* textSelection) {
		auto iter = commands_map.find(com);
		if (iter != commands_map.end()) {
			(*iter->second)(text, textSelection);
			//printf("\n%s", iter->first.c_str());
		}
	}

private:
	static void ev_backspace(Text* text, TextSelection* textSelection) {
		if (!text->content.empty()) {
			if (textSelection->selecting) {
				text->content.delete_text(textSelection->begin_point, textSelection->end_point);
				textSelection->selecting = false;
			}
			else {
				//lop off character
				text->content.buff_throw();
			}
			text->caret.move();
			text->caret.should_scroll_by_caret = true;
		}
	}

	static void ev_return(Text* text, TextSelection* textSelection) {
		if (textSelection->selecting) {
			text->content.delete_text(textSelection->begin_point, textSelection->end_point);
			textSelection->selecting = false;
		}
		text->append_to_content(NEWLINE);
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvr(Text* text, TextSelection* textSelection) {
		text->content.move_point_right();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvl(Text* text, TextSelection* textSelection) {
		text->content.move_point_left();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvu(Text* text, TextSelection* textSelection) {
		text->content.move_point_up();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_mvd(Text* text, TextSelection* textSelection) {
		text->content.move_point_down();
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_tab(Text* text, TextSelection* textSelection) {
		//text->content.buff_insert(TAB_INDENT);
		if (textSelection->selecting) {
			text->content.delete_text(textSelection->begin_point, textSelection->end_point);
			textSelection->selecting = false;
		}
		text->content.buff_insert(TAB_INDENT);
		//text->content.buff_insert(SPACES_INDENT);
		text->caret.move();
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_end(Text* text, TextSelection* textSelection) {
		text->content.move_point_to_end_line();
		text->caret.move();
		//text->caret.should_scroll_by_caret = true;
	}

	static void ev_home(Text* text, TextSelection* textSelection) {
		text->content.move_point_to_begin_line();
		text->caret.move();
		//text->caret.should_scroll_by_caret = true;
	}

	static void ev_paste(Text* text, TextSelection* textSelection) {
		if (textSelection->selecting) {
			text->content.delete_text(textSelection->begin_point, textSelection->end_point);
			textSelection->selecting = false;
		}
		if (SDL_HasClipboardText()) {
			auto cliptext = SDL_GetClipboardText();
			auto validclip = SDL::stripInvalidChar(std::string(cliptext));
			text->append_to_content(validclip);
			text->caret.move();
			SDL_free(cliptext);
		}
		text->caret.should_scroll_by_caret = true;
	}

	static void ev_copy(Text* text, TextSelection* textSelection) {
		if (textSelection->selecting) {
			auto sstr = text->content.copy_text(textSelection->begin_point, textSelection->end_point);
			SDL_SetClipboardText(sstr.c_str());
		}
	}

	static void ev_save(Text* text, TextSelection* textSelection) {
		int msgboxID =  MessageBox(
			NULL, 
			"Do you want to save change to untitled?", 
			"FEM SOLVER", MB_YESNOCANCEL
		);

		switch (msgboxID)
		{
		case IDCANCEL:
			// do nothing
			break;
		case IDNO:
			// TODO: add code
			break;
		case IDYES:
			// TODO: add code
			////Save Dialog
			//OPENFILENAME ofn;
			//char szFileName[MAX_PATH] = "";

			//ZeroMemory(&ofn, sizeof(ofn));

			//ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
			//ofn.hwndOwner = mHandle;
			//ofn.lpstrFilter = "Lua Scripts (*.lua)\0*.lua\0Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
			//ofn.lpstrFile = szFileName;
			//ofn.nMaxFile = MAX_PATH;
			//ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			//ofn.lpstrDefExt = "lua";
			//if (GetSaveFileName(&ofn)) {

			//}
			break;
		}
	}
};

			break;
		}
	}
};
