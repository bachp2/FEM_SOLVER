#pragma once
#include <cairo.h>
class Canvas {
private:
	SDL_Texture* texture;
	cairo_surface_t *cairo_surface;
	cairo_t* cr;
	void free_texture(SDL_Texture *t)
	{
		if (t != nullptr) {
			SDL_DestroyTexture(t);
		}
	}

public:
	virtual void init(SDL_Renderer* rend, SDL_Rect* target) {
		free_texture(texture);

		texture = SDL_CreateTexture(rend,
			SDL_PIXELFORMAT_ARGB8888,
			SDL_TEXTUREACCESS_STREAMING,
			target->w, target->h
		);
		void* pixels;
		int pitch;
		SDL_LockTexture(texture, NULL, &pixels, &pitch);
		cairo_surface = cairo_image_surface_create_for_data(
			(unsigned char*)pixels,
			CAIRO_FORMAT_ARGB32,
			target->w, target->h, pitch
			);
		cr = cairo_create(cairo_surface);
		cairo_set_source_rgb(cr, 1, 1, 1);
		cairo_paint(cr);
		cairo_set_source_rgb(cr, 0, 0, 0);
		auto ox = 1.0f*target->w / 2;
		auto oy = 1.0f*target->h / 2;
		for (int i = 0; i <= 10000; i++) {
			double x = ox + cos(2 * M_PI * i / 500) * 70 + cos(2 * M_PI * i / 10000) * 110;
			double y = oy + sin(2 * M_PI * i / 500) * 70 + sin(2 * M_PI * i / 10000) * 110;
			if (i == 0)
				cairo_move_to(cr, x, y);
			else
				cairo_line_to(cr, x, y);
		}
		cairo_close_path(cr);
		cairo_stroke(cr);

		SDL_UnlockTexture(texture);
	}

	virtual void render(SDL_Renderer* rend, SDL_Rect* target) {
		assert(texture != NULL);

		SDL::renderTexture(
			texture, rend, 
			target->x, target->y,
			target
		);
	};

	virtual void destroy() {
		free_texture(texture);
		cairo_destroy(cr);
		cairo_surface_destroy(cairo_surface);
	}
};