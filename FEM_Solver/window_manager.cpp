#include "Window_Manager.h"

//#include <vld.h>
HMENU mMenu;
HMENU mExit;
HMENU mFile;
HMENU mEdit;
HMENU mView;
HMENU mRun;
HWND mHandle;

void Window_Manager::loop()
{
	while (isRunning)
	{
		mouse.updateMouseState(&mHandle);
		handleEvents();
		handleInteractions();
		drawAndUpdate();
	}
}
int Window_Manager::init()
{
	luaEngine.init();
	// Allocate console for program output
	AllocConsole();
	freopen("CONOUT$", "w", stdout);

	// Init video
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		MessageBox(0, SDL_GetError(), "Error", MB_ICONEXCLAMATION | MB_OK);
		return 1;
	}

	// Create first window
	auto wWidth = 1024;
	auto wHeight = 620;
	mWindow = SDL_CreateWindow(
		"FEM SOLVER",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		wWidth, wHeight, SDL_WINDOW_SHOWN
		);

	SDL_SysWMinfo mInfo;
	SDL_VERSION(&mInfo.version);
	if (!SDL_GetWindowWMInfo(mWindow, &mInfo))
	{
		MessageBox(0, "SDL_GetWMInfo Error", "Error", MB_ICONEXCLAMATION | MB_OK);
		return 1;
	}

	mHandle = mInfo.info.win.window;
	mMenu = CreateMenu();
	mFile = CreateMenu();
	AppendMenu(mMenu, MF_POPUP, (UINT_PTR)mFile, "File");
	AppendMenu(mFile, MF_STRING, ID_New, "New");
	AppendMenu(mFile, MF_STRING, ID_Open, "Open...");
	AppendMenu(mFile, MF_STRING, ID_Save, "Save");
	AppendMenu(mFile, MF_STRING, ID_SaveAs, "Save As...");
	AppendMenu(mFile, MF_SEPARATOR, 0, NULL);
	AppendMenu(mFile, MF_STRING, ID_Quit, "Quit");
	
	mEdit = CreateMenu();
	AppendMenu(mMenu, MF_POPUP, (UINT_PTR)mEdit, "Edit");
	mView = CreateMenu();
	AppendMenu(mMenu, MF_POPUP, (UINT_PTR)mView, "View");

	SetMenu(mHandle, mMenu);
	SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);

	atexit(SDL_Quit);

	SDL_SetWindowResizable(mWindow, SDL_TRUE);
	//Create Renderer for main window
	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED);
	
	//to show caret blinking
	SDL_SetRenderDrawBlendMode(mRenderer, SDL_BLENDMODE_BLEND);


	//SDL_SetWindowFullscreen(gWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
	SDL_initFramerate(&fpsManager);
	SDL_setFramerate(&fpsManager, 60);

	// Init Text struct
	if (TTF_Init() < 0) {
		printf("Error: %s\n", TTF_GetError());
	}

	text.size = 13;
	text.default_color = SDLHEX(#020000);
	//text.default_color = { 0,0,0,255 };
	text.font = TTF_OpenFont("FiraCode-Regular.ttf", text.size);
	text.font_glyphs.init(mRenderer, text.font, SDL::hex2sdl("#ffffff"));
	assert(text.font != nullptr);

	//set cursor color
	text.caret.color = SDLHEX(#444444);

	text.caret.rect.w = 1;
	text.caret.rect.h = TTF_FontLineSkip(text.font);

	text.caret.setTextBufferPointer(&text.content);
	text.caret.setFontPointer(text.font);
	text.caret.setFocusWindowPointer(&text.focus_window);
	text.content.buff_insert(text_template);
	//text selection
	selectionText.font = text.font;
	selectionText.color = SDL::hex2sdl("#7B7111");
	selectionText.clientPaneWidth = &scriptingPane.rect.w;
	selectionText.focus_window = &text.focus_window;

	//start text input
	SDL_StartTextInput();

	// Create Panes for scripting and drawing
	
	SDL_GetWindowSize(mWindow, &wWidth, &wHeight);
	scriptingPane.color = SDLHEX(#FFFFEB);
	scriptingPane.render_flag = false;
	scriptingPane.rect.h = wHeight - LINESTATUS_HEIGHT;
	scriptingPane.rect.w = wWidth / 2 - paneDivider.rect.w / 2;
	scriptingPane.rect.x = 0;
	scriptingPane.rect.y = 0;
	scriptingPane.components["VerticalScrollBar"] = Pane::mrectptr(new VScrollBar(&scriptingPane, &text.focus_window));
	text.focus_window = scriptingPane.rect;
	text.focus_window.w -= SCROLLBAR_WIDTH;

	paneDivider.color = SDLHEX(#757599);
	paneDivider.rect.h = wHeight;
	paneDivider.rect.w = 2;
	paneDivider.rect.x = wWidth / 2 - paneDivider.rect.w / 2;
	paneDivider.rect.y = 0;

	lineStatus.color = SDLHEX(#DAFFFF);
	lineStatus.rect.w = scriptingPane.rect.w;
	lineStatus.rect.h = LINESTATUS_HEIGHT;
	lineStatus.rect.x = 0;
	lineStatus.rect.y = scriptingPane.rect.h;
	//Enable WinAPI Events Processing
	SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);

	drawingPane.color = { 255,255,235,255};
	drawingPane.rect.h = wHeight;
	drawingPane.rect.w = wWidth / 2 - paneDivider.rect.w / 2;
	drawingPane.rect.x = wWidth / 2 + paneDivider.rect.w / 2;
	drawingPane.rect.y = 0;
	//init canvas
	canvas.create_texture(mRenderer, &drawingPane.rect);
	//pass truss pointer to canvas object
	canvas.truss = &truss;
	//set world's offset from screen
	truss.offsetX = drawingPane.rect.w / 2;
	truss.offsetY = drawingPane.rect.h / 2;
	return 0;
}

void Window_Manager::close()
{
	// Stop text input
	SDL_StopTextInput();
	// Free mouse cursor
	mouse.free_cursor();
	mouse.cursor = nullptr;
	//Clean LUAJIT
	luaEngine.close();
	text.destroy();
	canvas.free_texture();
	// Close and destroy the window
	SDL_DestroyWindow(mWindow);
	mWindow = nullptr;

	
	// Free window surface
	//SDL_FreeSurface(wSurface);
	//wSurface = nullptr;

	// Destroy window renderer
	SDL_DestroyRenderer(mRenderer);
	mRenderer = nullptr;
	
	// Quit
	TTF_Quit();
	SDL_Quit();
	DestroyMenu(mMenu);
#ifdef DEBUGGING
	_CrtMemDumpAllObjectsSince(NULL);
#endif
}

void Window_Manager::drawAndUpdate()
{
	//internal call to draw background
	scriptingPane.render(mRenderer);
	//Draw Text to Scripting Pane
	
	text.free_texture(text.render_texture);
	text.render_texture = SDL_CreateTexture(
		mRenderer, SDL_PIXELFORMAT_RGBA8888,
		SDL_TEXTUREACCESS_TARGET,
		text.focus_window.w, text.focus_window.h
		);
	if (!text.render_texture) {
		printf("Texture Creation Failed");
	}
	
	SDL_SetRenderTarget(mRenderer, text.render_texture);
	SDL::SetRenderDrawColor(mRenderer, scriptingPane.color);
	SDL_RenderFillRect(mRenderer, &scriptingPane.rect);

	if (selectionText.selecting && selectionText.begin_point != selectionText.end_point)
	{
		selectionText.render(mRenderer);
	}
	else {
		SDL_Rect hightlight_at_caret = { 0, text.caret.rect.y - text.focus_window.y, scriptingPane.rect.w, text.caret.rect.h };
		SDL::SetRenderDrawColor(mRenderer, SDL::hex2sdl("#FDD6B1"));
		SDL_RenderFillRect(mRenderer, &hightlight_at_caret);
	}
	
	text.caret.render(mRenderer);
	text.draw_to_render_texture(mRenderer, scriptingPane, text.caret.should_scroll_by_caret);
	
	SDL_SetRenderTarget(mRenderer, NULL);
	SDL::renderTexture(text.render_texture, mRenderer, 0, 0, &text.focus_window);
	//draw entities
	//drawingPane.render(mRenderer);
	canvas.render(mRenderer, &drawingPane.rect);
	lineStatus.render(mRenderer);
	std::ostringstream s;
	s << "[untitled]" << "[cols:" 
	  << std::right << std::setw(3) << std::setfill(' ') 
	  << text.caret.current_column
	  << "~ln:"
	  << std::right << std::setw(4) << std::setfill(' ') 
	  << text.caret.current_line << "]"
	  << "[ASCII]" << "[^R to Run Script] fps: "
	  << SDL_getFramerate(&fpsManager);
	text.renderString(mRenderer, s.str(), lineStatus.rect.x, lineStatus.rect.y, SDLHEX(#281316));
	paneDivider.render(mRenderer);

	//truss.render(mRenderer);

	//push to front
	//// Clear the entire screen to our selected color.
	//SDL_RenderClear(mRenderer);
	SDL_RenderPresent(mRenderer);
	
	SDL_UpdateWindowSurface(mWindow);
}

void Window_Manager::handleEvents()
{
	const Uint8 *keyboard_state_array = SDL_GetKeyboardState(NULL);
	std::string combo;
	auto keymod = SDL_GetModState();
	SDL_Event ev;
	while (SDL_PollEvent(&ev) != 0) {
		switch (ev.type) {
		case SDL_QUIT:
			isRunning = false;
			break;
		case SDL_KEYDOWN:
			if (keymod & KMOD_CTRL) combo.append("Ctrl-");
			if (keymod & KMOD_ALT) combo.append("Alt-");
			if (keymod & KMOD_SHIFT) combo.append("Shift-");

			if(combo.empty())
				keysHandler.do_command(KEY_NAME(ev.key.keysym.sym), &text, &selectionText);
			else {
				combo.append(KEY_NAME(ev.key.keysym.sym));
				keysHandler.do_command(combo, &text, &selectionText);
			}

			if (keyboard_state_array[SDL_SCANCODE_LCTRL] && ev.key.keysym.sym == SDLK_r)
			{
				//truss.clear();
				truss.nodes_xy_position.clear();
				truss.elements.clear();
				luaEngine.run(text.content.str());

				IOFormat Fmt(StreamPrecision, DontAlignCols, ", ", ", ", "", "", " << ", ";");
				std::string sep = "\n----------------------------------------\n";
				mesh msh;
				msh.nodesxy = VectorXf::Map(truss.nodes_xy_position.data(), truss.nodes_xy_position.size());
				msh.elements = VectorXi::Map(truss.elements.data(), truss.elements.size());
				msh.node_forces = VectorXf::Zero(truss.nodes_xy_position.size());
				for (const auto& e : truss.forces) {
					msh.node_forces(2*e.first) = e.second[0];
					msh.node_forces(2 *e.first + 1) = e.second[1];
				}
				//printf("E:%.2f, A:%.2f\n", luaEngine.get_global_double("E"), luaEngine.get_global_double("A"));
				msh.props.mesh_fabrication = VectorXf::Ones(truss.elements.size() / 2);
				msh.props.mesh_material = VectorXf::Ones(truss.elements.size() / 2);
				msh.props.mesh_fabrication *= luaEngine.get_global_double("E");
				msh.props.mesh_material *= luaEngine.get_global_double("A");
				//msh.props.mesh_fabrication = 
				std::cout << msh.nodesxy.format(Fmt) << sep;
				std::cout << msh.elements.format(Fmt) << sep;
				std::cout << msh.node_forces.format(Fmt) << sep;
				std::cout << msh.props.mesh_fabrication.format(Fmt) << sep;
				std::cout << msh.props.mesh_material.format(Fmt) << sep;
				Solver vSolver;
				vSolver._mesh = msh;
				auto Km = vSolver.assemble_master_stiffness();
				IOFormat HeavyFmt(FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");
				printf("rows:%d,cols:%d\n", Km.rows(), Km.cols());
				std::cout << Km.format(HeavyFmt) << sep;
			}
			else if (keyboard_state_array[SDL_SCANCODE_LCTRL] && ev.key.keysym.sym == SDLK_s)
			{
				std::string filename = current_path() + "\\lua_scripts\\figure3-6a.lua";
				//printf(filename.c_str());
				std::ofstream ofile(filename);
				ofile << text.content.str();
				ofile.close();
			}
			else if (keyboard_state_array[SDL_SCANCODE_LCTRL] && ev.key.keysym.sym == SDLK_k) 
			{
				text.content.move_point_to_end_line();
				text.caret.move();
				selectionText.begin(text.caret.current_line, text.caret.current_column,
					text.content.get_point_index());
				selectionText.selecting = true;
					
				text.content.move_point_to_begin_line();
				text.caret.move();
				selectionText.end(text.caret.current_line, text.caret.current_column,
					text.content.get_point_index());
				
				auto sstr = text.content.copy_text(selectionText.begin_point, selectionText.end_point);
				SDL_SetClipboardText(sstr.c_str());
				text.content.delete_text(selectionText.begin_point, selectionText.end_point);
				text.caret.move();
				selectionText.selecting = false;
			}
			else if (keyboard_state_array[SDL_SCANCODE_LCTRL] && ev.key.keysym.sym == SDLK_l)
			{
				text.content.move_point_to_end_line();
				text.caret.move();
				selectionText.begin(text.caret.current_line, text.caret.current_column,
					text.content.get_point_index());
				selectionText.selecting = true;

				text.content.move_point_to_begin_line();
				text.caret.move();
				selectionText.end(text.caret.current_line, text.caret.current_column,
					text.content.get_point_index());

				auto sstr = text.content.copy_text(selectionText.begin_point, selectionText.end_point);
				SDL_SetClipboardText(sstr.c_str());
				text.caret.move();
				selectionText.selecting = false;
			}
			else if (keyboard_state_array[SDL_SCANCODE_LCTRL] && ev.key.keysym.sym == SDLK_a)
			{
				
			}
			break;
		case SDL_TEXTINPUT:
			//printf(ev.text.text);
			if (selectionText.selecting) {
				text.content.delete_text(selectionText.begin_point, selectionText.end_point);
				selectionText.selecting = false;
			}
			text.append_to_content(ev.text.text[0]);
			text.caret.move();
			break;
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEMOTION:
			//printf("%d\n", ev.type);
			mouse.state = handleMouseStates(ev.type, ev.button.button);
			break;
		case SDL_MOUSEWHEEL:
			if (ev.wheel.y > 0) // scroll up
			{
				text.scroll_up(scriptingPane);
			}
			else if (ev.wheel.y < 0) // scroll down
			{
				text.scroll_down(scriptingPane);
			}
			text.caret.should_scroll_by_caret = false;
			break;
		case SDL_SYSWMEVENT:
			handleWindowsAPICom(&ev);
			break;
		case SDL_WINDOWEVENT:
			switch (ev.window.event)
			{
			case SDL_WINDOWEVENT_RESIZED:
				rescale_mrects();
				break;
			case SDL_WINDOWEVENT_ENTER:
				mouse.inWindow = true;
				break;
			case SDL_WINDOWEVENT_LEAVE:
				mouse.inWindow = false;
				break;
			}
			break;
		}
	}
}

mRect* selected_gui_entity;
void Window_Manager::handleInteractions()
{
	auto vscrollbar = dynamic_cast<VScrollBar*>(scriptingPane.components["VerticalScrollBar"].get());
	if (mouse.state == MOUSEFREE) {
		SDL_CaptureMouse(SDL_FALSE);
		vscrollbar->thumb.color = SDLHEX(#5b5b4d);
		selected_gui_entity = nullptr; 
	}

	if (selected_gui_entity != nullptr)
	{
		if (selected_gui_entity == &paneDivider)
		{
			int rx;
			SDL_GetRelativeMouseState(&rx, NULL);
			selected_gui_entity->rect.x += rx;
			scriptingPane.rect.w += rx;
			drawingPane.rect.x += rx;
			drawingPane.rect.w -= rx;
			text.focus_window = scriptingPane.rect;
			lineStatus.rect.w = text.focus_window.w;
			text.focus_window.w -= SCROLLBAR_WIDTH;
			canvas.free_texture();
			canvas.create_texture(mRenderer, &drawingPane.rect);
			auto vscrollbar = dynamic_cast<VScrollBar*>(scriptingPane.components["VerticalScrollBar"].get());
			vscrollbar->rect.x += rx;
			vscrollbar->thumb.rect.x += rx;
			//vscrollbar->rescale_to_resized_client_pane();
		}
		else if (selected_gui_entity == &scriptingPane) {
			selectionText.selecting = true;
			if (mouse.posY == 0) {
				//capture mouse events outside SDL window
				SDL_CaptureMouse(SDL_TRUE);
				text.content.move_point_up();
				text.caret.move();
				text.caret.should_scroll_by_caret = true;
				Sleep(6);
			}
			else if (mouse.posY > text.focus_window.h) {
				//capture mouse events outside SDL window
				SDL_CaptureMouse(SDL_TRUE);
				text.content.move_point_down();
				text.caret.move();
				text.caret.should_scroll_by_caret = true;
				Sleep(6);
			}
			else
				text.caret.move(mouse.posX, mouse.posY);
			selectionText.end(text.caret.current_line, text.caret.current_column,
				text.content.get_point_index());
		}
		else {
			//selected gui entity is vertical scrollbar's thumb
			int ry;
			SDL_GetRelativeMouseState(NULL, &ry);
			if (ry > 0) // scroll up
			{
				int wy = (int)ry*scriptingPane.rect.h *1.0f / text.focus_window.h;
				text.scroll_down(scriptingPane, wy);
			}
			else if (ry < 0) // scroll down
			{
				int wy = (int)-ry*scriptingPane.rect.h *1.0f / text.focus_window.h;
				text.scroll_up(scriptingPane, wy);
			}
			text.caret.should_scroll_by_caret = false;
			//selected_gui_entity->rect.y += ry;
			/*text.focus_window.y = cround(selected_gui_entity->rect.y * 1.0f * scriptingPane.rect.h / text.focus_window.h);*/
			
		}
	}
	else if (drawingPane.isInRect(mouse.posX, mouse.posY))
	{
		mouse.setSystemCursor(SDL_SYSTEM_CURSOR_HAND);
		if (mouse.state == ON_CLICKED) {
			SDL_GetRelativeMouseState(NULL, NULL);
		}
		else if (mouse.state == ON_DRAGGED) {
			
			int rx,ry;
			SDL_GetRelativeMouseState(&rx, &ry);
			truss.offsetX += rx;
			truss.offsetY += ry;
			/*truss.world.x += rx;
			truss.world.y += ry;*/
		}
		else if (mouse.state == MOUSEFREE) {
			
		}
	}
	else if (paneDivider.isInRect(mouse.posX, mouse.posY))
	{
		mouse.setSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE);
		if (mouse.state == ON_CLICKED)
		{
			selected_gui_entity = &paneDivider;
			SDL_GetRelativeMouseState(NULL, NULL);
		}
	}
	else if (lineStatus.isInRect(mouse.posX, mouse.posY))
	{
		mouse.setSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
	}
	else if (vscrollbar->isInRect(mouse.posX, mouse.posY))
	{
		mouse.setSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
		if (vscrollbar->thumb.isInRect(mouse.posX, mouse.posY) && mouse.state == ON_CLICKED)
		{
			selected_gui_entity = &vscrollbar->thumb;
			SDL_GetRelativeMouseState(NULL, NULL);
		}
		else if (vscrollbar->thumb.isInRect(mouse.posX, mouse.posY))
			vscrollbar->thumb.color = SDLHEX(#adad83);
	}
	else
	{
		mouse.setSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
		if (mouse.state == ON_CLICKED)
		{
			text.caret.move(mouse.posX, mouse.posY);
			selectionText.begin(text.caret.current_line, text.caret.current_column, 
								text.content.get_point_index());
			if (selectionText.selecting)  selectionText.selecting = false;
		}
		else if (mouse.state == ON_DRAGGED)
		{
			selected_gui_entity = &scriptingPane;
		}
	}

	
}

void Window_Manager::handleWindowsAPICom(SDL_Event* ev) {
	if (ev->syswm.msg->msg.win.msg == WM_COMMAND)
	{
		auto ID = LOWORD(ev->syswm.msg->msg.win.wParam);
		if (ID == ID_Quit)
		{
			isRunning = false;
		}
		else if (ID == ID_New)
		{
			text.content.buff_clear();
			text.caret.move();
			text.caret.should_scroll_by_caret = true;
		}
		else if (ID == ID_Open)
		{
			OPENFILENAME ofn;
			char szFileName[MAX_PATH] = "";

			ZeroMemory(&ofn, sizeof(ofn));

			ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
			ofn.hwndOwner = mHandle;
			ofn.lpstrFilter = "Lua Scripts (*.lua)\0*.lua\0Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
			ofn.lpstrFile = szFileName;
			ofn.nMaxFile = MAX_PATH;
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			ofn.lpstrDefExt = "lua";
			if (GetOpenFileName(&ofn))
			{
				// Do something usefull with the filename stored in szFileName 
				std::ifstream t(szFileName);
				std::stringstream buffer;
				buffer << t.rdbuf();
				text.content.buff_clear();
				text.content.buff_insert(buffer.str());
				t.close();
			}
		}
		else if (ID == ID_SaveAs) {
			//Save Dialog
			OPENFILENAME ofn;
			char szFileName[MAX_PATH] = "";

			ZeroMemory(&ofn, sizeof(ofn));

			ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
			ofn.hwndOwner = mHandle;
			ofn.lpstrFilter = "Lua Scripts (*.lua)\0*.lua\0Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
			ofn.lpstrFile = szFileName;
			ofn.nMaxFile = MAX_PATH;
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			ofn.lpstrDefExt = "lua";
			if (GetSaveFileName(&ofn)) {

			}
		}
	}
}

MouseBehavior Window_Manager::handleMouseStates(Uint32 mouseEvent, Uint8 btn_type)
{
	switch (mouse.state) {
	case MOUSEFREE:
		if (btn_type == SDL_BUTTON_LEFT) {
			if (mouseEvent == SDL_MOUSEBUTTONDOWN)
				mouse.state = ON_CLICKED;
		}
		break;
	case ON_CLICKED:
		if (mouseEvent == SDL_MOUSEBUTTONUP)
			mouse.state = MOUSEFREE;
		else if (mouseEvent == SDL_MOUSEMOTION) mouse.state = ON_DRAGGED;
		break;
	case ON_DRAGGED:
		if (mouseEvent == SDL_MOUSEBUTTONUP)
			mouse.state = MOUSEFREE;
		break;
	}
	return mouse.state;
}
