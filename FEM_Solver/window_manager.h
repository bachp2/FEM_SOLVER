#pragma once
/*
Load appropriate windows api lib for user interface
*/
#include "headers.h"
#include <map>
#include <string>
#include "mrect.h"
#include "text.h"
#include "text_selection.h"
#include "keys_handler.h"
#include "caret.h"
#include <Eigen/Dense>
#include "solver.h"
#include "textbuffer.h"
#include "framerate.h"
#include "utils.h"
#include "canvas.h"
#include "constants.h"
#include "lua_engine.h"
#include "gui.h"
//#define DEBUGGING
#ifdef DEBUGGING

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>  
#include <crtdbg.h> 

#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif // !_DEBUG

#endif // !DEBUGGING

extern Truss truss;

class Window_Manager
{
public:
	int init();
	void close();
	void handleEvents();
	void handleInteractions();
	void drawAndUpdate();
	void loop();
	Window_Manager() {};
	~Window_Manager() {};

	SDL_Window *mWindow = nullptr;
	SDL_Renderer *mRenderer = nullptr;
	SDL_Color backgroundColor = { 255, 255, 255, 255 };
	Mouse mouse;
	Text text;
private:
	void rescale_mrects()
	{
		int ww, wh;
		SDL_GetWindowSize(mWindow, &ww, &wh);
		int oldww, oldwh;
		oldww = scriptingPane.rect.w + paneDivider.rect.w + drawingPane.rect.w;
		oldwh = paneDivider.rect.h;
		float hscale = ww*1.0f / oldww;
		float vscale = wh*1.0f / oldwh;

		SDL::scale(paneDivider.rect, hscale, vscale);
		SDL::scale(scriptingPane.rect, hscale, vscale);
		SDL::scale(drawingPane.rect, hscale, vscale);
		SDL::scale(lineStatus.rect, hscale, vscale);
		SDL::scale(text.focus_window, hscale, vscale);
		auto vscrollbar = dynamic_cast<VScrollBar*>(scriptingPane.components["VerticalScrollBar"].get());
		SDL::scale(vscrollbar->rect, hscale, vscale);
		SDL::scale(vscrollbar->thumb.rect, hscale, vscale);
		canvas.free_texture();
		canvas.create_texture(mRenderer, &drawingPane.rect);
	}

	MouseBehavior handleMouseStates(Uint32 mouseEvent, Uint8 btn_type);
	void handleWindowsAPICom(SDL_Event* ev);
	bool isRunning = true;
	FPSmanager fpsManager;
	Pane scriptingPane;
	Pane drawingPane;
	mRect paneDivider;
	mRect lineStatus;
	LuaEngine luaEngine;
	TextSelection selectionText;
	KeysHandler keysHandler;
	Canvas canvas;
	/*const std::string text_template{
		"-- FEM template\nunit(2)\nA = 1.5 --in2\nE = 10e6 --psi\n--structure geometric definition\n--assign node numbers\nnode(0,0)\nnode(0,40)\nnode(40,0)\nnode(40,40)\nnode(80,0)\nnode(80,40)\n-- define element connectivity \nele(2,4)\nele(3,5)\nele(1,3)\nele(1,4)\nele(4,3)\nele(4,5)\nele(4,6)\nele(6,5)\nforce(3,{0,-2000})\nforce(5,{2000,0})\nforce(6,{4000,6000})\n--structure mechanical definition\n\n-- solver\n\n-- post-processing"
	};*/
	const std::string text_template{
	"-- FEM template\nunit(2)\nA = 1.5 --in2\nE = 10e6 --psi\n--structure geometric definition\n--assign node numbers\nnode(0,0)\nnode(0,40)\nnode(40,40)\n\n-- define element connectivity \nele(1,3)\nele(2,3)\nforce(3,{300,500})\n--structure mechanical definition\n\n-- solver\n\n-- post-processing"
	};
};

