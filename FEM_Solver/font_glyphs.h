#pragma once
#include <SDL_ttf.h>
#include <SDL.h>
#include <iostream>
struct FontGlyphs {
public:
	void init(SDL_Renderer* rend, TTF_Font* f, SDL_Color color) {
		font = f;
		char ASCII_printable_set[128 - 33];
		int index = 0;
		for (int i = 33; i < 127; ++i)
		{
			ASCII_printable_set[index++] = char(i);
		}
		ASCII_printable_set[index] = '\0';

		glyphsAtlas = render_text_to_texture(rend, ASCII_printable_set, color);
		TTF_GlyphMetrics(font, 'd', NULL, NULL, NULL, NULL, &advance);
		fontLineSkip = TTF_FontLineSkip(font);
		glyphQuad = { 0,0,advance,fontLineSkip };
		clipQuad = { 0,0,advance,fontLineSkip };
	}

	void renderGlyph(SDL_Renderer* rend, int c, int x, int y) {
		glyphQuad.x = x;
		glyphQuad.y = y;
		if (c >= 33 && c < 127) {
			clipQuad.x = (c - 33)*advance;
			clipQuad.y = 0;
			SDL_RenderCopy(rend, glyphsAtlas, &clipQuad, &glyphQuad);
		}
	}

	void renderGlyph(SDL_Renderer* rend, int c, int x, int y, SDL_Color color) {
		glyphQuad.x = x;
		glyphQuad.y = y;
		if (c >= 33 && c < 127) {
			SDL_SetTextureColorMod(glyphsAtlas, color.r, color.g, color.b);
			clipQuad.x = (c - 33)*advance;
			clipQuad.y = 0;
			SDL_RenderCopy(rend, glyphsAtlas, &clipQuad, &glyphQuad);
			//reset color modding
			SDL_SetTextureColorMod(glyphsAtlas, 255, 255, 255);
		}
	}

	void resetQuad() {
		glyphQuad.x = 0;
		glyphQuad.y = 0;
	}

	~FontGlyphs() {
		free_texture(glyphsAtlas);
	}
private:
	int advance;
	int fontLineSkip;
	TTF_Font *font;
	SDL_Rect glyphQuad;
	SDL_Rect clipQuad;
	SDL_Texture* glyphsAtlas;
	
	void free_texture(SDL_Texture *t)
	{
		if (t != nullptr) {
			SDL_DestroyTexture(t);
		}
	}

	SDL_Texture* render_text_to_texture(SDL_Renderer *rend, const char* input, SDL_Color colour)
	{
		//Get rid of preexisting texture
		//SDL_Surface* textSurface = TTF_RenderUTF8_Blended(font, input.c_str(), colour);
		SDL_Surface* textSurface = TTF_RenderText_Blended(font, input, colour);
		SDL_Texture* texture;
		SDL_Rect rect;
		if (textSurface != nullptr)
		{
			//Create texture from surface pixels
			texture = SDL_CreateTextureFromSurface(rend, textSurface);
			if (texture == NULL)
			{
				printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
			}
			else
			{
				//Get image dimensions
				rect.x = 0;
				rect.y = 0;
				rect.w = textSurface->w;
				rect.h = textSurface->h;
			}

			//Get rid of old surface
			SDL_FreeSurface(textSurface);
			return texture;
		}
		return nullptr;
	}
};
