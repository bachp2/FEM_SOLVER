#pragma once
#include <cstdlib>
#include <string>
#include <assert.h>
#define DEFAULT_BUFFER_SIZE 16
#define TESTING_MODE

class TextBuffer
{
	char* buffer;
	char* point;
	char* gapstrt;
	char* gapend;
	size_t size;
	size_t capacity;
public:
	TextBuffer();
	~TextBuffer();
	
	void move_point_left();
	void delete_text(int pstart, int pend);
	std::string copy_text(int pstart, int pend);
	void move_point_right();

	void move_point_up();

	void move_point_down();

	void focus_gap_on_point();

	int buff_insert(char ch);

	int buff_insert(std::string str);

	int buff_throw();

	void buff_clear() {
		point = &buffer[0];
		gapstrt = point;
		gapend = &buffer[capacity - 1];
		size = 0;
	}

	bool isFull() { return size == capacity; }

	bool isNotFull() { return size != capacity; }

	size_t get_capacity() { return capacity; }

	size_t get_size() { return size; }

	bool empty() { return size == 0; }

	char* get_point() {
		return point;
	}

	std::string str();;

#ifdef TESTING_MODE
	size_t get_gap_size() { return gapend - gapstrt + 1; }
	size_t get_point_index() { return point - &buffer[0]; }
	size_t get_gap_start_index() { return gapstrt - &buffer[0]; }
	size_t get_gap_end_index() { return gapend - &buffer[0]; }
	std::string str_with_gap();;
#endif // TESTING_MODE

	void move_point(unsigned int nln, unsigned int ncol, unsigned int cln);

	void move_point_to_begin_line();

	void move_point_to_end_line();

	void move_caret_to_point(unsigned int& cln, unsigned int& ccol);

private:
#ifndef TESTING_MODE
	size_t get_gap_size() { return gapend - gapstrt + 1; }
	size_t get_point_index() { return point - &buffer[0]; }
	size_t get_gap_start_index() { return gapstrt - &buffer[0]; }
	size_t get_gap_end_index() { return gapend - &buffer[0]; }
#endif // !TESTING_MODE
	void clamp(char *& cptr) {
		if (cptr < begin()) cptr = begin();
		else if (cptr >= end()) cptr = end() - 1;
	}
	int seekc_backward(char ch, char*& cptr);
	int seekc_forward(char ch, char*& cptr);
	void buff_expand(int newslots);
	char *begin() { return &buffer[0]; }
	char *end() { return &buffer[capacity]; }
	
};

