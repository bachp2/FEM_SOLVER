#pragma once
#include <SDL.h>
#include <vector>
#include <unordered_map>
struct Truss
{
	double offsetX{};
	double offsetY{};
	double unit{100.0};
	double E{};
	double A{};
	std::vector<float> nodes_xy_position;
	std::vector<int> elements;//specified by pair of nodes
	std::unordered_map < int, std::vector<double> > forces;
private:
	float arrow_length{ 10.0f };
	float arrow_angle{0.3f};
	std::string force_unit{"KN"};
	friend class Canvas;
};