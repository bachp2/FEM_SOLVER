#define ID_Menu	100
#define ID_Quit	103
#define ID_New  104
#define ID_Open 105
#define ID_Save 106
#define ID_SaveAs 107
#define ID_Undo 108
#define ID_Redo 109

#define ID_Cut 110
#define ID_Paste 111
#define ID_Copy 112
#define ID_Delete 113
#define ID_Find 114
#define ID_FindNext 115
#define ID_Replace 116
#define ID_SelectAll 117
#define ID_DTime 118
#define ID_About 119
#define ID_WordWrap 120
#define ID_Macros 121

