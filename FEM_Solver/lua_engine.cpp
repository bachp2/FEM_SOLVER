#include "lua_engine.h"
#include <iostream>
Truss truss;

inline int PrintOut(lua_State *L, std::ostream& out);
inline int lua_Print(lua_State *L);
/*
Lua Print overload
*/

int lua_PushTrussNode(lua_State* L) {
	float x = lua_tonumber(L, 1);
	float y = lua_tonumber(L, 2); // get function arguments
	truss.nodes_xy_position.push_back(x);
	truss.nodes_xy_position.push_back(y);
	/*auto j = truss.nodes_xy_position.size() / 2;
	if (j > 1) {
		truss.elements.push_back(j - 2);
		truss.elements.push_back(j - 1);
	}*/
	return 1;
}

int lua_setUnit(lua_State* L) {
	double i = lua_tonumber(L, 1);
	truss.unit = i;
	return 1;
}

int lua_setMatFab(lua_State* L) {
	double A = lua_tonumber(L, 1);
	truss.A = A;
	return 1;
}

int lua_setMechProp(lua_State* L) {
	truss.E = (double) lua_tonumber(L, 1);
	return 1;
}

int lua_setForce(lua_State* L) {
	int index = lua_tonumber(L, 1);
	size_t len = lua_objlen(L, 2);
	if(index > truss.nodes_xy_position.size()/2  || index < 1)
		return luaL_error(L, "index out of bound");

	if (len != 2) {
		return luaL_error(L, "array length required %d, got %d)",
			2, len);
	}

	double force[2] = { 0 };
	lua_rawgeti(L, 2, 1);
	force[0] = lua_tonumber(L, -1);
	lua_rawgeti(L, 2, 2);
	force[1] = lua_tonumber(L, -1);
	std::vector<double> v;
	v.push_back(force[0]);
	v.push_back(force[1]);
	truss.forces[index - 1] = v;
	auto f = truss.forces[index - 1];
	//printf("i:%d, <%.2f,%.2f>\n", index, f[0], f[1]);
	return 1;
}

//TODO: throw error for LUA to catch
int lua_ConnectNodes(lua_State* L) {
	int i = lua_tonumber(L, 1) - 1;
	int j = lua_tonumber(L, 2) - 1; // get function arguments
	////printf("world {%d %d %d %d}", WorldScene::world.x, WorldScene::world.y, world.w, world.h);
	truss.elements.push_back(i);
	truss.elements.push_back(j);
	return 1;
}

int PrintOut(lua_State *L, std::ostream& out)
{
	int n = lua_gettop(L);  /* number of arguments */
	if (!n)
		return 0;
	int i;
	lua_getglobal(L, "tostring");
	for (i = 1; i <= n; i++)
	{
		const char *s;
		lua_pushvalue(L, -1);  /* function to be called */
		lua_pushvalue(L, i);   /* value to print */
		lua_call(L, 1, 1);
		s = lua_tostring(L, -1);  /* get result */
		if (s == NULL)
			return luaL_error(L, LUA_QL("tostring") " must return a string to "
				LUA_QL("print"));
		if (i > 1) out << "\t";
		out << s;
		lua_pop(L, 1);  /* pop result */
	}
	out << std::endl;
	return 0;
}

int lua_Print(lua_State *L)
{
	return PrintOut(L, std::cout);
}

void LuaEngine::init()
{
	L = lua_open();
	// Load the libraries
	luaL_openlibs(L);
	lua_getglobal(L, "_G");
	lua_register(L, "print", &lua_Print);
	lua_register(L, "node", &lua_PushTrussNode);
	lua_register(L, "ele", &lua_ConnectNodes);
	lua_register(L, "force", &lua_setForce);
	lua_register(L, "unit", &lua_setUnit);
	lua_register(L, "set_surface_area", &lua_setMatFab);
	lua_register(L, "set_mech_prop", &lua_setMechProp);
	lua_pop(L, 1);
}

void LuaEngine::run(std::string& script)
{
	system("CLS");
	int exit_code = luaL_dostring(L, script.c_str());
	if(exit_code == 1) 
		printf("%s\n", lua_tostring(L, -1));
}

double LuaEngine::get_global_double(const std::string& var)
{
	lua_getglobal(L, var.c_str());
	double ret = lua_tonumber(L, -1);
	lua_pop(L, 1);
	return ret;
}

void LuaEngine::close()
{
	lua_gc(L, LUA_GCCOLLECT, 0);
	lua_close(L);
	L = 0;
}
