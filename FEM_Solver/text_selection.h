#pragma once
#include "mrect.h"
#include "constants.h"
#include <SDL_ttf.h>
struct TextSelection: mRect{
	int begin_col, end_col;
	int begin_ln, end_ln;
	int begin_point, end_point;
	TTF_Font* font;
	SDL_Rect* focus_window;
	bool selecting = false;
	int* clientPaneWidth;
	void begin(int ln, int col, int b) {
		begin_ln = ln;
		begin_col = col;
		begin_point = b;
	}

	void end(int ln, int col, int e) {
		end_ln = ln;
		end_col = col;
		end_point = e;
	}

	virtual void render(SDL_Renderer *rend)
	{
		int advance;
		TTF_GlyphMetrics(font, 'd', NULL, NULL, NULL, NULL, &advance);
		const int FONT_LINE_SKIP = TTF_FontLineSkip(font);
		if (selecting)
		{
			if (begin_ln == end_ln) {
				int min, max;
				minmax(begin_col, end_col, &min, &max);
				rect.x = LEFT_MARGIN + advance*(min - 1);
				rect.y = TOP_MARGIN + (begin_ln - 1)*FONT_LINE_SKIP - focus_window->y;
				rect.w = advance*(max - min);
				rect.h = TTF_FontLineSkip(font);
				mRect::render(rend);
			}
			else {
				//this is a hack (needs comments!)
				int lnmin, lnmax, cmin, cmax;
				minmax(begin_ln, end_ln, &lnmin, &lnmax);
				if (lnmin == begin_ln) {
					cmin = begin_col;
					cmax = end_col;
				}
				else {
					cmin = end_col;
					cmax = begin_col;
				}
				//selecting hightlight rect at begin line
				rect.x = LEFT_MARGIN + advance*(cmin - 1);
				rect.y = TOP_MARGIN + (lnmin - 1)*FONT_LINE_SKIP - focus_window->y;
				rect.w = *clientPaneWidth;
				rect.h = FONT_LINE_SKIP;
				mRect::render(rend);

				//selecting hightlight rect in between
				
				rect.x = LEFT_MARGIN;
				rect.y = TOP_MARGIN + lnmin*FONT_LINE_SKIP - focus_window->y;
				rect.w = *clientPaneWidth;
				rect.h = (lnmax - lnmin - 1)*FONT_LINE_SKIP;
				mRect::render(rend);

				//selecting hightlight rect at end line
				rect.x = LEFT_MARGIN;
				rect.y = TOP_MARGIN + (lnmax-1)*FONT_LINE_SKIP - focus_window->y;
				rect.w = advance*(cmax - 1);
				rect.h = FONT_LINE_SKIP;;
				mRect::render(rend);
			}
		}
	}
};
