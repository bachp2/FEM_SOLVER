#pragma once
constexpr int SCROLLBAR_WIDTH = 10;
constexpr int DIVIDER_WIDTH = 2;
constexpr int LEFT_MARGIN = 10;
constexpr int TOP_MARGIN = 3;
constexpr int LINESTATUS_HEIGHT = 14;
#define NEWLINE '\n'
#define TAB_INDENT '\t'
#define TABSIZE 3
#define	SPACES_INDENT "    "
